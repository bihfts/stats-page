var stats = [
	{
		title: "Cocaine Blues",
		book: 1,
		episode: 1,
		nipple : 3,
		breast: 10,
		partners: 1,
		nowKiss: 0,
		squee: 0
	},
	{
		title: "Flying Too High",
		book: 2,
		episode: -1,
		nipple: 2,
		breast: 7,
		partners: 2,
		nowKiss: 0,
		squee: 0
	},
	{
		title: "Murder on the Ballarat Train",
		book: 3,
		episode: 2,
		nipple: 1,
		breast: 6,
		partners: 1,
		nowKiss: 1,
		squee: 1
	},
	{
		title: "Death at Victoria Dock",
		book: 4,
		episode: 5,
		nipple: 0,
		breast: 4,
		partners: 1,
		nowKiss: 1,
		squee: 6
	}
]

let isBook = u => u.book !== -1;
let isEpisode = u => u.episode !== -1;

let sortedBooks = () => stats.filter(isBook).sort((a,b) => a.book - b.book);
let sortedTv = () => stats.filter(isEpisode).sort((a,b) => a.episode - b.episode);
