#!/bin/bash

HOST=$1  #This is the FTP servers host or IP address.
USER=$2   #This is the FTP user that has access to the server.
PASS=$3          #This is the password for the FTP user.

# Call 1. Uses the ftp command with the -inv switches. 
#-i turns off interactive prompting. 
#-n Restrains FTP from attempting the auto-login feature. 
#-v enables verbose and progress. 

ftp -invp $HOST << EOF
user $USER $PASS
ascii
mput *.js
mput *.css
mput *.html
bye
EOF