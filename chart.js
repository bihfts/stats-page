var ctx = document.getElementById("bookChart").getContext('2d');
var myChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: stats.filter(isBook).map(u => u.title),
		datasets: [{
			label: '# Nipple',
			data: stats.filter(isBook).map(u => u.nipple),
			backgroundColor: [
				'rgba(255, 99, 132, 0.0)'
			],
			borderColor: [
				'rgba(255,99,132,1)'
			],
			borderWidth: 2
		}, {
			label: '# Breast',
			data: stats.filter(isBook).map(u => u.breast),
			backgroundColor: [
				'rgba(132, 99, 255, 0.0)'
			],
			borderColor: [
				'rgba(132,99,255,1)'
			],
			borderWidth: 2
		}, {
			label: '# Partners',
			data: stats.filter(isBook).map(u => u.partners),
			backgroundColor: [
				'rgba(132, 255, 99, 0.0)'
			],
			borderColor: [
				'rgba(132,255,99,1)'
			],
			borderWidth: 2
		}]
	},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero: true
				}
			}]
		}
	}
});

var ctx = document.getElementById("tvChart").getContext('2d');
var myChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: sortedTv().map(u => u.title),
		datasets: [{
			label: '# Now Kiss',
			data: sortedTv().map(u => u.nowKiss),
			backgroundColor: [
				'rgba(255, 99, 132, 0.0)'
			],
			borderColor: [
				'rgba(255,99,132,1)'
			],
			borderWidth: 2
		}, {
			label: '# Squee',
			data: sortedTv().map(u => u.squee),
			backgroundColor: [
				'rgba(132, 99, 255, 0.0)'
			],
			borderColor: [
				'rgba(132,99,255,1)'
			],
			borderWidth: 2
		}]
	},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero: true
				}
			}]
		}
	}
});
